﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucene.Net.Index;
using Lucene.Net.Store;
using System.IO;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;

namespace WebSamples
{
    public class LuceneIndexer
    {
        private readonly HttpServerUtilityBase _server;
        private static bool _indexed = false;
        private static object _syncObject = new object();

        public LuceneIndexer(HttpServerUtilityBase server)
        {
            _server = server;
        }

        private Lucene.Net.Store.Directory GetDirectory()
        {
            var dir = _server.MapPath("~/Content/features/lucene/AspnetWebStackIndex");
            return FSDirectory.Open(new DirectoryInfo(dir));
        }

        private Analyzer GetAnalyzer()
        {
            return new LuceneFilePathAnalyzer();
        }

        public void EnsureIndex()
        {
            if (!_indexed)
            {
                lock (_syncObject)
                {
                    if (!_indexed)
                    {
                        _indexed = true;
                        using (var writer = new IndexWriter(GetDirectory(), GetAnalyzer(), true, IndexWriter.MaxFieldLength.UNLIMITED))
                        {
                            var path = _server.MapPath("~/Content/features/lucene/aspnetwebstack-files.txt");
                            foreach (var line in System.IO.File.ReadAllLines(path))
                            {
                                var doc = new Document();
                                doc.Add(new Field("path", line, Field.Store.YES, Field.Index.ANALYZED));
                                writer.AddDocument(doc);
                            }
                        }
                    }
                }
            }
        }
    }
}