﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSamples
{
    [AttributeUsage(AttributeTargets.Method)]
    public class NavigateableAttribute : Attribute
    {
        public const string DefaultYear = "Active";
        public string Name { get; set; }
        public IEnumerable<string> Topics { get; set; }
        public string Year { get; set; } = DefaultYear;

        public NavigateableAttribute(string name, params string[] topics)
        {
            this.Name = name;
            this.Topics = topics;
        }

        public NavigateableAttribute(string name, int year, params string[] topics)
            : this(name, topics)
        {
            this.Year = year.ToString();
        }
    }
}