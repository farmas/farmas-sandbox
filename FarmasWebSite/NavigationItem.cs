﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSamples
{
    public class NavigationItem
    {
        public string Section { get; set; }
        public string Tab { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool IsActive { get; set; }
        public string RouteName { get; set; }
        public object RouteValues { get; set; }
        public IEnumerable<NavigationItem> SubItems { get; set; } = new List<NavigationItem>();

        public NavigationItem Clone()
        {
            return new NavigationItem()
            {
                ActionName = this.ActionName,
                ControllerName = this.ControllerName,
                IsActive = this.IsActive,
                Section = this.Section,
                RouteName = this.RouteName,
                Tab = this.Tab,
                RouteValues = this.RouteValues,
                SubItems = this.SubItems.Select(i => i.Clone()).ToArray()
            };
        }
    }
}