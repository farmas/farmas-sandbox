﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSamples
{
    public static class Topics
    {
        public const string MyPackages = "My Packages";
        public const string Aurelia = "Aurelia.js";
        public const string AspNet = "ASP.NET";
        public const string AspNetCore = "ASP.NET Core";
        public const string UnCategorized = "Uncategorized";
        public const string Express = "Express.js";
        public const string Meteor = "Meteor.js";
        public const string Cardboard = "Cardboard.js";
        public const string Lucene = "Lucene.NET";
        public const string Playground = "Playground";
        public const string SignalR = "SignalR";
        public const string MongoDB = "MongoDB";
        public const string React = "React.js";
        public const string Monaco = "Monaco.js";
        public const string Angular = "Angular.js";
        public const string NestJs = "Nest.js";

        public static readonly Dictionary<string, int> Order = new Dictionary<string, int>()
        {
            { MyPackages, 10 },
            { AspNet, 20 },
            { AspNetCore, 30 },
            { SignalR, 35 },
            { Express, 40 },
            { NestJs, 45 },
            { Meteor, 50 },
            { Angular, 55 },
            { Aurelia, 60 },
            { React, 65 },
            { Monaco, 67 },
            { Cardboard, 70 },
            { MongoDB, 75 },
            { Lucene, 80 },
            { Playground, 100 },
            { UnCategorized, 1000 }
        };
    }
}