﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using System.Collections.Generic;
using System.Web.Configuration;

[assembly: OwinStartupAttribute(typeof(WebSamples.OwinStartup))]
namespace WebSamples
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseKentorOwinCookieSaver();
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            var appSettings = WebConfigurationManager.AppSettings;
            var authOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = appSettings["GoogleClientId"],
                ClientSecret = appSettings["GoogleClientSecret"],
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive
            };

            authOptions.Scope.Add("email");

            app.UseGoogleAuthentication(authOptions);
        }
    }
}