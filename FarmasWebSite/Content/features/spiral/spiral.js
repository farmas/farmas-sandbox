﻿(function(global, $, undefined) {

    $.widget("farmas.spiral", {
        options: {
            text: null,
            degrees: 10
        },

        _text: null,
        _degrees: null,

        _create: function() {
            this.element.addClass("farmas-spiral");

            this._setOption("text", this.options.text);
            this._setOption("degrees", this.options.degrees);

            this._setSpiral();
        },

        destroy: function() {
            this.element
                .empty()
                .removeClass("farmas-spiral");
        },

        _setOption: function(key, val) {
            if (key === "text") {
                this._text = val;
            }
            else if (key === "degrees") {
                this._degrees = parseInt(val);
            }

            this._setSpiral();
        },

        _setSpiral: function() {
            var i, li, rotation,
                start = Math.floor(this._text.length / 2) * this._degrees * -1;

            this.element.empty();

            for (i = 0, li = this._text.length; i < li; i++) {
                rotation = "rotate(" + (start + (this._degrees * i)) + "deg)";
                $("<span>")
                    .text(this._text[i])
                    .css({
                        "transform": rotation,
                        "-ms-transform": rotation,
                        "-webkit-transform": rotation,
                        "-moz-transform": rotation,
                        "-o-transform": rotation
                    })
                    .appendTo(this.element);
            }
        }
    });

})(this, jQuery);