﻿$(document).ready(function () {
    var cardCount = 0;
    var cardTitles = [
        "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent dapibus ullamcorper ipsum, sit amet hendrerit enim.",
        "Nam non scelerisque arcu. Nam varius facilisis magna, molestie ultrices nisl. Suspendisse semper convallis justo, sed porta velit sollicitudin id.",
        "In eget vestibulum metus. Aliquam id est ac metus malesuada volutpat quis quis justo. Donec varius vitae erat nec vehicula."
    ];
    var teamMembers = ["Alvaro", "Brad", "Chris", "David", "Federico", "Mo", "Steve", "Tom", "William"];
    var statuses = ["Todo", "In Progress", "Done"];
    var priorities = [0, 1, 2];
    var groupByList = [
    {
        name: "Owner",
        cardProperty: "owner",
        cardValues: teamMembers
    },
    {
        name: "Priority",
        cardProperty: "priority"
    }];


    function getTestCards() {
        var cards = [];

        for (var i = 0; i < 100; i++) {
            cards.push(_createCard());
        }

        return cards;
    }

    function _getRandomValue(arr) {
        var size = arr.length,
            index = Math.floor(Math.random() * size);

        return arr[index];
    }

    function _createCard() {
        return {
            id: (++cardCount).toString(),
            title: _getRandomValue(cardTitles),
            owner: _getRandomValue(teamMembers),
            priority: _getRandomValue(priorities),
            status: _getRandomValue(statuses)
        };
    }

    $("#board").cardboard({
        boards: [
        {
            id: "Default",
            groupByList: groupByList,
            columns: [
                {
                    name: "",
                    width: "10%"
                },
                {
                    name: "Todo",
                    width: "40%",
                    showCellCount: true
                },
                {
                    name: "In Progress",
                    width: "25%"
                },
                {
                    name: "Done",
                    width: "25%"
                }
            ]
        },
        {
            id: "Secondary",
            groupByList: groupByList,
            columns: [
                {
                    name: "",
                    width: "10%"
                },
                {
                    name: "Todo",
                    width: "40%",
                    showCellCount: true
                },
                {
                    name: "In Progress",
                    width: "25%"
                },
                {
                    name: "In Review",
                    width: "25%"
                }
            ]
        }],
        getCardsAsync: function () {
            var cards = getTestCards();

            return $.Deferred().resolve(cards).promise();
        }
    });
});


/*
var groupByList: BoardRowsGroupBy[] = [
        {
            name: "Owner",
            cardProperty: "owner",
            cardValues: TestData.teamMembers
        },
        {
            name: "Priority",
            cardProperty: "priority"
        }]

    Board.init($("#board"), <BoardCollectionSettings>{
        getCardsAsync: (): JQueryPromise<BoardCardData[]> => {
            var deferred = $.Deferred<any>();

            window.setTimeout(() => {
                deferred.resolve(TestData.getTesCards());
            }, 1000);

            return deferred.promise();
        },
        boards: [
            {
                id: "Default",
                groupByList: groupByList,
                columns: [
                    {
                        name: "",
                        width: "10%"
                    },
                    {
                        name: "Todo",
                        width: "40%",
                        showCellCount: true
                    },
                    {
                        name: "In Progress",
                        width: "25%"
                    },
                    {
                        name: "Done",
                        width: "25%"
                    }
                ]
            },
            {
                id: "Secondary",
                groupByList: groupByList,
                columns: [
                    {
                        name: "",
                        width: "10%"
                    },
                    {
                        name: "Todo",
                        width: "40%",
                        showCellCount: true
                    },
                    {
                        name: "In Progress",
                        width: "25%"
                    },
                    {
                        name: "In Review",
                        width: "25%"
                    }
                ]
            }]
    });
*/