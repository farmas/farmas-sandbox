﻿using System.Linq;
using System.Globalization;
using System.Web.Mvc;
using WebSamples.WeatherService;

namespace WebSamples.Controllers
{
    public class WeatherController : Controller
    {
        private static WeatherDescription[] _weatherInfo;

        [OutputCache(Duration=300, VaryByParam="zip")]
        public ActionResult GetWeather(int zip)
        {
            var client = new WeatherSoapClient("WeatherSoap");
            var weather = client.GetCityWeatherByZIP(zip.ToString(CultureInfo.InvariantCulture));

            if (_weatherInfo == null)
            {
                _weatherInfo = client.GetWeatherInformation();
            }

            var info = _weatherInfo.First(i => i.WeatherID == weather.WeatherID);
            return Json(new
                            {
                                city = weather.City,
                                state = weather.State,
                                temperature = weather.Temperature,
                                success = weather.Success,
                                url = info == null ? "http://ws.cdyne.com/WeatherWS/Images/na.gif" : info.PictureURL
                            }
                        , JsonRequestBehavior.AllowGet);
        }
    }
}
