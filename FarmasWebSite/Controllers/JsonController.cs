﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Threading;

namespace WebSamples.Controllers
{
    public class JsonController : Controller
    {
        private Random _random = new Random();
        private char[] _letters = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', };

        public ActionResult GetContent(int start = 1, int count = 100)
        {
            Thread.Sleep(1500);
            var series = Enumerable.Range(start, count).Select(i => new { id = i, name = GetRandomName() }); ;
            return Json(series, JsonRequestBehavior.AllowGet);
        }

        private string GetRandomName()
        {
            var name = new StringBuilder();
            for (int i = 0; i < 3; i++)
            {
                var segment = GetRandomString();
                name.Append(segment[0].ToString().ToUpperInvariant());
                name.Append(segment.Substring(1));
                if (i < 2)
                {
                    name.Append(" ");
                }
            }
            
            return name.ToString();
        }

        private string GetRandomString()
        {
            var s = new StringBuilder();

            for (int i = 0; i < 10; i++)
            {
                s.Append(_letters[_random.Next(_letters.Count())]);
            }

            return s.ToString();
        }
    }
}
