﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSamples.Controllers
{
    public class AboutController : Controller
    {
        public ViewResult About()
        {
            return View();
        }
    }
}
