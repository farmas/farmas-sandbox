﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebSamples.Controllers
{
    public class AuthController : Controller
    {
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider)
        {
            return new AuthChallengeActionResult(provider, Url.Action("ExternalLoginCallback"));
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            var loginInfo = await authenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo != null)
            {
                TempData.Add("email", loginInfo.Email);
                TempData.Add("name", loginInfo.DefaultUserName);
            }

            return RedirectToAction("Authentication", "Projects");
        }
    }
}