﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSamples.Models.ViewModels;

namespace WebSamples.Controllers
{
    public class ProjectsController : Controller
    {
        [Navigateable("Atlassian .NET SDK", Topics.MyPackages)]
        public ActionResult JiraSDK()
        {
            this.ViewBag.Title = "Packages - Atlassian .NET SDK";
            return View("Packages/JiraSDK");
        }

        [Navigateable("VSJira", Topics.MyPackages)]
        public ActionResult VSJira()
        {
            this.ViewBag.Title = "Packages - VSJira";
            return View("Packages/VSJira");
        }

        [Navigateable("Cardboard.js", 2015, Topics.MyPackages)]
        public ActionResult Cardboard()
        {
            this.ViewBag.Title = "Packages - Cardboard.js";
            return View("Packages/Cardboard");
        }

        [Navigateable("Seattle Library Extension", Topics.MyPackages)]
        public ActionResult SPLExtension()
        {
            this.ViewBag.Title = "Packages - Seattle Library Chrome Extension";
            return View("Packages/SPLExtension");
        }

        [Navigateable("Test Automation Framework (LTAF)", 2010, Topics.MyPackages)]
        public ActionResult LTAF()
        {
            this.ViewBag.Title = "Packages - LTAF";
            return View("Packages/LTAF");
        }

        [Navigateable("Battleship", 2015, Topics.AspNet, Topics.SignalR)]
        public ActionResult Battleship()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Battleship",
                Summary = @"Play a classic game of battleship with a remote friend.
 Web site uses Owin to authenticate with Google provider and SignalR for
 live communication between players.",
                SourceUrl = "https://bitbucket.org/farmas/battleship",
                ExternalSite = "http://battle-ship.azurewebsites.net/",
                YearCreated = 2015
            };
            return View("ExternalContent");
        }

        [Navigateable("Daycare", 2017, Topics.AspNetCore, Topics.Aurelia, Topics.MongoDB)]
        public ActionResult Daycare()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Daycare",
                Summary = @"Web application that models a daycare or small school, including classes, teachers, students, their families. Built with ASP.NET Core, Aurelia and MongoDB.",
                SourceUrl = "https://bitbucket.org/farmas/daycare",
                ExternalSite = "http://daycare-sample.herokuapp.com/",
                YearCreated = 2017
            };
            return View("ExternalContent");
        }

        [Navigateable("Timeline", 2018, Topics.AspNetCore, Topics.Angular, Topics.MongoDB)]
        public ActionResult Timeline()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Timeline",
                Summary = @"Web application that allows users to create and share visual time lines of events. Built with ASP.NET Core, Angular 2 and MongoDB.",
                SourceUrl = "https://bitbucket.org/farmas-team/timeline-angular",
                ExternalSite = "https://timeline-sample.herokuapp.com/",
                YearCreated = 2018
            };
            return View("ExternalContent");
        }

        [Navigateable("Type Code", 2016, Topics.Meteor, Topics.MongoDB, Topics.React, Topics.Monaco)]
        public ActionResult TypeCode()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Type Code",
                Summary = "Application to help practice touch typing for developers. Users create lessons of any remote code file to practice touch typing and track their results. Uses React.js, Monaco.js, BlazeCSS, MongoDB and Google Auth.",
                SourceUrl = "https://bitbucket.org/farmas/typecode.com",
                ExternalSite = "http://type-code.herokuapp.com",
                YearCreated = 2016
            };
            return View("ExternalContent");
        }

        [Navigateable("Shared Calendar", 2016, Topics.Express)]
        public ActionResult SharedCalendar()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Shared Calendar",
                Summary = "Express.js application that uses Google Calendar API to allow authenticated users to create events on a shared calendar.",
                SourceUrl = "https://bitbucket.org/farmas/seattlelegalcalendar.com/src",
                ExternalSite = "http://seattlelegalcalendar.azurewebsites.net/",
                YearCreated = 2016
            };
            return View("ExternalContent");
        }

        [Navigateable("FriendTracker", 2019, Topics.NestJs, Topics.Aurelia, Topics.MongoDB)]
        public ActionResult FriendTracker()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Friend Tracker",
                Summary = @"Web application that helps to remind you to contact your old friends and collegues. Built with Nest.js, Aurelia, Azure Functions and MongoDB. Integrates with Google Contacts to import friends.",
                SourceUrl = "https://bitbucket.org/farmas-team/friend-tracker-web",
                ExternalSite = "https://myfriendstracker.herokuapp.com/",
                YearCreated = 2019
            };
            return View("ExternalContent");
        }

        [Navigateable("Lunch Time", 2016, Topics.Meteor, Topics.MongoDB)]
        public ActionResult LunchTime()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Lunch Time",
                Summary = "Meteor application to create lunch orders from recipes and ingredients. Can authenticate with google account, send mail of shopping list and share recipes.",
                SourceUrl = "https://bitbucket.org/farmas/lunch-time",
                ExternalSite = "https://mylunchtime.herokuapp.com/",
                YearCreated = 2016
            };
            return View("ExternalContent");
        }

        [Navigateable("Sample Board", 2014, Topics.Cardboard)]
        public ActionResult CardboardSample()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "Team 'scrum' board that can be used with any back end written in typescript and require.js. This fully client-side sample auto-generates card data without any backend.",
                SourceUrl = "https://bitbucket.org/farmas/cardboard",
                YearCreated = 2014
            };
            return View("Cardboard/SimpleBoard");
        }

        [Navigateable("Jira Board", 2014, Topics.Cardboard)]
        public ActionResult CardboardJenkins()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Cardboard - Jenkins",
                Summary = "Sample cardboard that reads data from a live Jira backend, in this case the public Jenkins jira instance. This sample shows how to hook up the cardboard to a backend.",
                SourceUrl = "https://bitbucket.org/farmas/cardboard-server-jira",
                ExternalSite = "http://cardboard-jenkins.azurewebsites.net/",
                YearCreated = 2014
            };
            return View("ExternalContent");
        }

        [Navigateable("ASP.NET Source", 2013, Topics.Lucene)]
        public ActionResult Lucene()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "Sample that uses Lucene.Net to index the file paths of the Asp.Net Web Stack source using a custom filter and analyzer.",
                SourceUrl = "https://bitbucket.org/farmas/farmas-sandbox/src/master/FarmasWebSite/Controllers/LuceneController.cs",
                YearCreated = 2013
            };
            return View("Lucene/Lucene");
        }

        [Navigateable("Snake 1 Player", 2012, Topics.Playground)]
        public ActionResult Single()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "A snake game using HTML5 canvas. Written as a weekend project using pixel inspection instead of data structures for engine.",
                SourceUrl = "https://bitbucket.org/farmas/farmas-sandbox/src/master/FarmasWebSite/Content/features/canvas-snake/canvas-snake.js",
                YearCreated = 2012
            };
            return View("Canvas/Single-Player");
        }

        [Navigateable("Snake 2 Players", 2012, Topics.Playground)]
        public ActionResult Multi()
        {
            return View("Canvas/Multi-Player");
        }

        [Navigateable("OAuth", 2015, Topics.Playground)]
        public ActionResult Authentication()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "Sample authentication workflow with external provider using Owin.",
                YearCreated = 2015
            };
            return View("Authentication/OAuth");
        }

        [Navigateable("Hangman", 2014, Topics.AspNet)]
        public ActionResult Hangman()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Hangman",
                Summary = "Web site where users can pose word challenges to other members to guess in a hangman game.",
                SourceUrl = "https://bitbucket.org/farmas/hangman/src",
                ExternalSite = "http://hangman.apphb.com/",
                YearCreated = 2014
            };
            return View("ExternalContent");
        }

        [Navigateable("Leo Mejor", 2011, Topics.Playground)]
        public ActionResult LeoMejor()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Title = "Leo Mejor",
                Summary = "Word guessing game (in spanish) for children written in Backbone.js. Wrote this for my sister who is a teacher and is building some resources for her class. Used it to learn backbone, underscore and jasmine.",
                SourceUrl = "https://bitbucket.org/farmas/leomejor/src",
                ExternalSite = "http://leomejor.apphb.com/App.html",
                YearCreated = 2011
            };
            return View("ExternalContent");
        }

        [Navigateable("Weather", 2012, Topics.Playground)]
        public ViewResult Weather()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "A simple weather app using backbone.js and MVC backend. Can add/remove cities and change temperature units. Written as a weekend project for fun.",
                SourceUrl = "https://bitbucket.org/farmas/farmas-sandbox/src/master/FarmasWebSite/Content/features/weather/weather.js",
                YearCreated = 2012
            };
            return View("Ajax/Weather");
        }

        [Navigateable("Infinite Scrolling", 2012, Topics.Playground)]
        public ActionResult InfiniteScrolling()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "Sample that uses jQuery to implement an infinite loading list.",
                SourceUrl = "https://bitbucket.org/farmas/farmas-sandbox/src/master/FarmasWebSite/Views/Projects/Ajax/Infinite-Scrolling.cshtml",
                YearCreated = 2012
            };

            return View("Ajax/Infinite-Scrolling");
        }

        [Navigateable("Spiral", 2012, Topics.Playground)]
        public ActionResult Spiral()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "JQuery widget that displays text in a spiral.",
                SourceUrl = "https://bitbucket.org/farmas/farmas-sandbox/src/master/FarmasWebSite/Content/features/spiral/",
                YearCreated = 2012
            };

            return View("Ajax/Spiral");
        }

        [Navigateable("CC.NET Dashboard", 2012, Topics.Playground)]
        public ActionResult CCNetDashboard()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "Dashboard for CruiseControl.NET that updates as the projects change status.",
                SourceUrl = "https://bitbucket.org/farmas/farmas-sandbox/src/master/FarmasWebSite/Views/Projects/Ajax/ccnet-dashboard.cshtml",
                YearCreated = 2012
            };

            return View("Ajax/ccnet-dashboard");
        }

        [Navigateable("Final Fantasy XIII-2", 2012, Topics.Playground)]
        public ActionResult FFXIII2()
        {
            this.ViewBag.Overview = new SampleOverview()
            {
                Summary = "A solution to the clock paradox from Final Fantasy XIII-2.",
                SourceUrl = "https://bitbucket.org/farmas/farmas-sandbox/src/master/FarmasWebSite/Content/features/ffxiii-2/paradox-solver.js",
                YearCreated = 2012
            };
            return View("Algorithms/FFXIII-2");
        }
    }
}
