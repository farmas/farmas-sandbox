﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebSamples
{
    public class RouteConfig
    {
        public class RouteNames
        {
            public const string Topics = "Topics";
            public const string Years = "Years";
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "DeprecatedProjects",
                "Projects/{action}",
                new { controller = "Projects", action = "JiraSDK" }
            );

            routes.MapRoute(
                RouteNames.Topics,
                "Topics/{topic}/{action}",
                new { controller = "Projects", action = "JiraSDK", topic = "TBD" }
            );

            routes.MapRoute(
                RouteNames.Years,
                "Years/{year}/{action}",
                new { controller = "Projects", action = "JiraSDK", year = "Active" }
            );

            routes.MapRoute(
                "Default",
                "{controller}/{action}",
                new { controller = "Projects", action = "JiraSDK" }
            );
        }
    }
}
