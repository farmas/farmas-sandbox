﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using WebSamples.Models.ViewModels;
using WebSamples.Controllers;

namespace WebSamples
{
    public class NavigationFilter : IActionFilter
    {
        private static IEnumerable<NavigationItem> _topicNavigations;
        private static IEnumerable<NavigationItem> _yearNavigations;

        private class ExpandedNavigationItem
        {
            public NavigateableAttribute MenuItem { get; set; }
            public string ControllerName { get; set; }
            public string ActionName { get; set; }
        }

        static NavigationFilter()
        {
            LoadNavigationItems();
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                var currentMenuItem = filterContext.ActionDescriptor
                    .GetCustomAttributes(typeof(NavigateableAttribute), true)
                    .Cast<NavigateableAttribute>()
                    .FirstOrDefault();

                if (currentMenuItem == null)
                {
                    currentMenuItem = new NavigateableAttribute("Unknown", Topics.UnCategorized);
                }

                NavigationItem[] navigations;
                var currentTab = currentMenuItem.Name;
                string currentSection;
                var routeName = RouteConfig.RouteNames.Topics;
                var yearRouteData = filterContext.RouteData.Values["year"] as string;
                var topicRouteData = filterContext.RouteData.Values["topic"] as string;

                if (!String.IsNullOrEmpty(yearRouteData))
                {
                    navigations = _yearNavigations.Select(n => n.Clone()).ToArray();
                    currentSection = yearRouteData;
                    routeName = RouteConfig.RouteNames.Years;
                }
                else
                {
                    navigations = _topicNavigations.Select(n => n.Clone()).ToArray();
                    currentSection = String.IsNullOrEmpty(topicRouteData) ? currentMenuItem.Topics.First() : topicRouteData;
                }

                var activeSection = navigations.FirstOrDefault(nav => String.Equals(nav.Section, currentSection, StringComparison.InvariantCultureIgnoreCase));
                var tabs = activeSection != null ? activeSection.SubItems : new List<NavigationItem>();

                if (activeSection != null)
                {
                    activeSection.IsActive = true;
                    var activeTab = activeSection.SubItems.FirstOrDefault(nav => String.Equals(nav.Tab, currentTab, StringComparison.InvariantCultureIgnoreCase));

                    if (activeTab != null)
                    {
                        activeTab.IsActive = true;
                    }
                }

                result.ViewBag.Sections = navigations;
                result.ViewBag.Tabs = tabs;
                result.ViewBag.RouteName = routeName;
            }
        }

        private static IEnumerable<NavigationItem> GetNavigationItemsByTopics(IEnumerable<ExpandedNavigationItem> navigations)
        {
            var topicNavigations = navigations.SelectMany(item => item.MenuItem.Topics.Select(topic => new NavigationItem()
            {
                Section = topic,
                RouteName = RouteConfig.RouteNames.Topics,
                RouteValues = new { action = item.ActionName, topic },
                Tab = item.MenuItem.Name,
                ControllerName = item.ControllerName,
                ActionName = item.ActionName,
            }));

            var sectionsMap = new Dictionary<string, List<NavigationItem>>();
            foreach (var navigation in topicNavigations)
            {
                if (!sectionsMap.ContainsKey(navigation.Section))
                {
                    sectionsMap.Add(navigation.Section, new List<NavigationItem>());
                }

                sectionsMap[navigation.Section].Add(navigation);
            }

            var sections = sectionsMap.Keys.OrderBy(section => Topics.Order[section]).Select(section =>
            {
                var firstItem = sectionsMap[section].First();
                return new NavigationItem()
                {
                    Section = section,
                    Tab = firstItem.Tab,
                    RouteName = RouteConfig.RouteNames.Topics,
                    RouteValues = new { action = firstItem.ActionName, topic = section },
                    ControllerName = firstItem.ControllerName,
                    ActionName = firstItem.ActionName,
                    SubItems = sectionsMap[section]
                };
            });

            return sections.ToArray();
        }

        private static IEnumerable<NavigationItem> GetNavigationItemsByYear(IEnumerable<ExpandedNavigationItem> navigations)
        {
            var yearNavigations = navigations.Select(item => new NavigationItem()
            {
                Section = item.MenuItem.Year,
                Tab = item.MenuItem.Name,
                RouteName = RouteConfig.RouteNames.Years,
                RouteValues = new { action = item.ActionName, year = item.MenuItem.Year },
                ControllerName = item.ControllerName,
                ActionName = item.ActionName,
            });

            var sectionsMap = new Dictionary<string, List<NavigationItem>>();
            foreach (var navigation in yearNavigations)
            {
                if (!sectionsMap.ContainsKey(navigation.Section))
                {
                    sectionsMap.Add(navigation.Section, new List<NavigationItem>());
                }

                sectionsMap[navigation.Section].Add(navigation);
            }

            var sections = sectionsMap.Keys.OrderBy(section => section).Reverse().Select(section =>
            {
                var firstItem = sectionsMap[section].First();
                return new NavigationItem()
                {
                    Section = section,
                    Tab = firstItem.Tab,
                    RouteName = RouteConfig.RouteNames.Years,
                    RouteValues = new { action = firstItem.ActionName, year = section },
                    ControllerName = firstItem.ControllerName,
                    ActionName = firstItem.ActionName,
                    SubItems = sectionsMap[section]
                };
            });

            return sections.ToArray();
        }

        private static void LoadNavigationItems()
        {
            var actions = typeof(ProjectsController).GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            var menuItems = actions.Select(action =>
                {
                    var menuItem = action.GetCustomAttribute(typeof(NavigateableAttribute), true) as NavigateableAttribute;

                    return new ExpandedNavigationItem()
                    {
                        MenuItem = menuItem ?? new NavigateableAttribute(action.Name, Topics.UnCategorized),
                        ControllerName = action.DeclaringType.Name.Replace("Controller", ""),
                        ActionName = action.Name
                    };
                });

            _topicNavigations = GetNavigationItemsByTopics(menuItems);
            _yearNavigations = GetNavigationItemsByYear(menuItems);
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }
    }
}