﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSamples.Models.ViewModels
{
    public class SampleOverview
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string SourceUrl { get; set; }
        public string ExternalSite { get; set; }
        public int YearCreated { get; set; }
    }
}
